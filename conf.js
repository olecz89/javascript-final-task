exports.config = {
    directConnect: true,
    //seleniumAddress: 'http://localhost:4444/wd/hub', //overriden by directConnect
    // set to "custom" instead of cucumber.
    framework: 'custom',

    // path relative to the current config file
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    // require feature files
    specs: ['./features/protractor.feature'],

    allScriptsTimeout: 150000,
    getPageTimeout: 30000,
    setDefaultTimeout: 120000,

    cucumberOpts: {
        // require step definitions
        require: [
            './features/steps/spec.js',
            './features/steps/given.js',
            './features/steps/when.js',
            './features/steps/then.js'
        ],
        format: 'pretty'
    },
    onPrepare: function () {
        browser.manage().window().maximize(); // maximize the browser before executing the feature files
    }
};