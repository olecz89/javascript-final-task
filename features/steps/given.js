'use strict';
const trip = require('../../functions/tripCreation');
const world = require("../../functions/world");

module.exports = function () {

    this.Given(/^I found international trip and I am logged in to myRyanair$/, function () {
        return new trip();
    });
};