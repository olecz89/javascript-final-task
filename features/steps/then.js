const paymentPart = require('../../functions/paymentFunctions');
'use strict';


module.exports = function () {

    this.Then(/^I try to pay with incorrect card and I see the booking has failed$/, function () {
        return new paymentPart();
    });
};
