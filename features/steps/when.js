'use strict';
const potentialPart = require('../../functions/potentialTripPart');

module.exports = function () {
    this.When(/^I proceed with booking to payment page$/, function () {
        return new potentialPart();
    });
};