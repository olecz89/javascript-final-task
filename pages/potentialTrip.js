require("protractor-cucumber-framework");
require("protractor");
const lok = require("../elements/locators");
const EC = protractor.ExpectedConditions;
'use strict';


const potentialTrip = function () {

    const locators = new lok();

    this.checkOutButton = function () {
        locators.potentialTrip().checkOut.click();
        browser.wait(EC.visibilityOf(locators.potentialTrip().popUp), 4000);
        locators.potentialTrip().closePopUp.click();
        return browser.wait(EC.elementToBeClickable(locators.paymentPage().firstName), 5000);
    };


    this.chooseSeats = function () {
        browser.sleep(9000);
        browser.wait(EC.invisibilityOf(locators.potentialTrip().nudgeBody), 35000);
        locators.potentialTrip().seatsExtras.click();
        browser.wait(EC.visibilityOf(locators.potentialTrip().seatMapRows), 5000);

        locators.potentialTrip().seats1.first().click();
        browser.wait(EC.visibilityOf(locators.potentialTrip().seatSelected), 4000);

        locators.potentialTrip().nextButton.click();

        browser.sleep(2000);
        locators.potentialTrip().sameSeats.isPresent().then(function (state) {
            console.log('state: ' + state);
            if (state === true) {
                return locators.potentialTrip().sameSeats.click();
            }
            else {
                locators.potentialTrip().seats2.click();
                return locators.potentialTrip().nextButton.click();
            }
        })

        // browser.wait(EC.or(locators.potentialTrip().notSameSeats.click(), locators.potentialTrip().seats2.click()), 6000);
        // browser.wait(EC.visibilityOf(locators.potentialTrip().notSameSeats), 5000);

        browser.wait(EC.visibilityOf(locators.potentialTrip().confirmSeatsTitle), 4000);
        locators.potentialTrip().confirmButton.click();
        browser.wait(EC.visibilityOf(locators.potentialTrip().declinePB), 4000);

        return locators.potentialTrip().declinePB.click();
    };

};

module.exports = potentialTrip;