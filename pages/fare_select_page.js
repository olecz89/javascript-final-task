require("protractor-cucumber-framework");
require("protractor");
const lok = require("../elements/locators");
const EC = protractor.ExpectedConditions;
'use strict';


const fareSelectPage = function () {

    const locators = new lok();

    this.assertFareSelect  = function () {
        return browser.wait(EC.visibilityOf(locators.fareSelectPage().outgoingFlight), 5000);
    };

    this.selectFlight = function () {
        return locators.fareSelectPage().outgoingFlight.isPresent().then(function () {
            locators.fareSelectPage().outgoingFlight.click();
            return browser.wait(EC.elementToBeClickable(locators.fareSelectPage().fareTypeOutgoing), 5000);
        })
    };

    this.selectFareOutgoing = function () {
        locators.fareSelectPage().fareTypeOutgoing.click();
        return browser.wait(EC.elementToBeClickable(locators.fareSelectPage().returnFlight), 2000);
    };


    this.selectReturnFlight = function () {
        return locators.fareSelectPage().returnFlight.isPresent().then(function () {
            locators.fareSelectPage().returnFlight.click();
            return browser.wait(EC.visibilityOf(locators.fareSelectPage().fareReturn), 2000);
        })
    };

    this.selectFareReturn = function () {
        return locators.fareSelectPage().fareReturn.click();
    };

    this.continueButton = function () {
        return locators.fareSelectPage().buttonContinue.click();
        // return browser.wait(EC.visibilityOf(locators.potentialTrip().extrasSection), 5000);
    };
};


module.exports = fareSelectPage;