require("protractor-cucumber-framework");
require("protractor");
const world = require("../functions/world");
const lok = require("../elements/locators");
const EC = protractor.ExpectedConditions;
'use strict';

const homePage = function () {
    const locators = new lok();


    this.getHomePage = function () {
        return browser.get('http://sit-aem.ryanair.com/ie/en/');
    };

    this.logIn = function (user, password) {
        locators.homePage().loginButton.click();
        let email = locators.homePage().email;
        email.sendKeys(user);
        locators.homePage().passwordField.sendKeys(password);
        locators.homePage().passwordField.sendKeys(protractor.Key.ENTER);
        return browser.wait(EC.visibilityOf(locators.homePage().city.get(1)), 2000);
    };

    // this.enterCity = function (index, cityName) {
    //     locators.homePage().city.get(index);
    //     return locators.homePage().city.clear().then(function () {
    //         locators.homePage().city.sendKeys(cityName);
    //         return locators.homePage().city.sendKeys(protractor.Key.TAB);
    //     })
    // };

    this.enterCity = function (locator, cityName) {
        return locator.clear().then(function () {
            locator.sendKeys(cityName);
            return locator.sendKeys(protractor.Key.TAB);
        })
    };


    this.enterFlight = function (string, flightDate) {
        const dateString = 'dateRange.';
        const fromDay = element(by.model(dateString.concat(string))).element(by.model('date[0]'));
        const fromMonth = element(by.model(dateString.concat(string))).element(by.model('date[1]'));
        const fromYear = element(by.model(dateString.concat(string))).element(by.model('date[2]'));
        const day = flightDate.slice(-2);
        const month = flightDate.slice(5, 7);
        const year = flightDate.slice(0, 4);
        fromDay.sendKeys(day);
        fromMonth.sendKeys(month);
        return fromYear.sendKeys(year);
    };


    this.searchForFlight = function () {
        locators.homePage().searchButton.click();
        return browser.wait(EC.visibilityOf(locators.fareSelectPage().flightHeader.get(0)), 6000);
    };
};

module.exports = homePage;






