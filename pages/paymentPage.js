require("protractor-cucumber-framework");
require("protractor");
const assert = require("chai").assert;
const lok = require("../elements/locators");
const EC = protractor.ExpectedConditions;
'use strict';

const paymentPage = function () {


    const locators = new lok();


    this.insertPersonalInfo = function () {
        return locators.paymentPage().firstField.click().then(function () {
            return locators.paymentPage().firstDropdown.click();
        });
    };

    this.paymentInfo = function () {
        locators.paymentPage().cardNum.sendKeys("5100000014101198");
        locators.paymentPage().cardType.click();
        locators.paymentPage().expMonth.click();
        locators.paymentPage().expYear.click();
        locators.paymentPage().cvvCode.sendKeys("123");
        return locators.paymentPage().cardName.sendKeys("Auto Test");
    };

    this.billingAddress = function () {
        locators.paymentPage().address1.sendKeys("21 Sun Lane");
        locators.paymentPage().address2.sendKeys("Sun Land");
        locators.paymentPage().city.sendKeys("Dublin");
        locators.paymentPage().postalCode.sendKeys("12345");
        locators.paymentPage().countryDropdown.click();
        locators.paymentPage().agreeButton.sendKeys(protractor.Key.SPACE);
        return locators.paymentPage().payNowButton.click();
    };


    this.endTest = function () {
        locators.paymentPage().payNowButton.click();
        browser.wait(EC.visibilityOf(locators.paymentPage().errorMessage2), 10000);
        return assert.exists(locators.paymentPage().errorMessage2);
        // return browser.sleep(3000);
    }


};

module.exports = paymentPage;