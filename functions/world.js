const world = this;

this.tripType = null;
this.origin = null;
this.destination = null;
this.flightDate = null;
this.returnDate = null;
this.userEmail = null;
this.userPass = null;
this.city = null;
this.foh = null;
this.fareSelectPage = null;
this.potentialTrip = null;
this.paymentPage = null;
this.beforeEach = null;
