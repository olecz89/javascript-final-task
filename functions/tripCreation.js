'use strict';
const rp = require("request-promise-native");
const homePage = require('../pages/home_page');
const world = require('../functions/world');

const createTripAndSearchFlight = function () {

     const createTrip = function() {
        return tripCreation().then(values => {
            world.tripType = values.bookingConstraint;
            world.origin = values.outBoundJourney.origin;
            world.destination = values.outBoundJourney.destination;
            world.flightDate = values.outBoundJourney.flightDate;
            world.returnDate = values.inBoundJourney.flightDate;
            console.log("Flying from " + world.origin + " to " + world.destination + " on " + world.flightDate + " and return on " + world.returnDate);
        });
    };

    const tripCreation = function () {
        const trip = {
            method: "POST",
            uri: "http://10.11.17.50:5050/trip",
            body: {
                "bookingConstraint":"international",
                "checkinConstraint":"none",
                "fareType":"standard",
                "tripType":"return",
                "adults":"1",
                "children":"0",
                "teens":"0",
                "infants":"0"
            },
            resolveWithFullResponse: true,
            json: true
        };
        return rp(trip)
            .then(function (value) {
                return value.body;
            })
    };

    const searchFlight = function () {
        world.foh.enterCity(0, world.origin);
        world.foh.enterCity(1, world.destination);
        world.foh.enterFlight("startDate", world.flightDate);
        world.foh.enterFlight("endDate", world.returnDate);
        return world.foh.searchForFlight();
    };

    function signupAndActivation() {
        const credentials = {
            method: "POST",
            uri: "http://10.11.17.50:5050/signupAndActivation",
            body: {
                "env": "sit",
                "random": true
            },
            resolveWithFullResponse: true,
            json: true
        };
        return PromisesHandler(credentials);
    };

    function PromisesHandler(handleThis){
        return rp(handleThis)
            .then(function (value) {
                return value.body;
            })
            .catch(function (err) {
                console.log("POST failed");
            });
    };

    function signUserIn() {
        return signupAndActivation().then(values => {
            world.userEmail = values.email;
            world.userPass = values.password;
            console.log("User mail: " + world.userEmail + " and user pass: " + world.userPass);
        });
    }

    return Promise.all([signUserIn(), createTrip()]).then(function () {
        world.foh = new homePage();
        world.foh.getHomePage();
        return world.foh.logIn(world.userEmail, world.userPass);
    });
};

module.exports = createTripAndSearchFlight;