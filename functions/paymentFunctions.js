'use strict';
const paymentPage = require('../pages/paymentPage');
const world = require("../functions/world");


const payFunctions = function () {

    function paymentInfoAndBooking() {
        world.paymentPage = new paymentPage();
        world.paymentPage.insertPersonalInfo();
        world.paymentPage.paymentInfo();
        return world.paymentPage.billingAddress();
    }

    function assertBookingHasFailed() {
        world.paymentPage.paymentInfo();
        return world.paymentPage.endTest();
    }

    return paymentInfoAndBooking().then(assertBookingHasFailed());

    // return Promise.resolve(paymentInfoAndBooking()).then(assertBookingHasFailed);
};

module.exports = payFunctions;