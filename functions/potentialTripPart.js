'use strict';
const homePage = require("../pages/home_page");
const fareSelect = require("../pages/fare_select_page");
const potential = require("../pages/potentialTrip");
const world = require('../functions/world');
const lok = require("../elements/locators");
const EC = protractor.ExpectedConditions;


const potentialTripAndExtras = function () {
    const locators = new lok();

    function frontOfHouse() {
        world.foh.enterCity(locators.homePage().originCity, world.origin);
        world.foh.enterCity(locators.homePage().destinationCity, world.destination);
        world.foh.enterFlight("startDate", world.flightDate);
        world.foh.enterFlight("endDate", world.returnDate);
        return world.foh.searchForFlight();
    }

    function flightSelection() {
        world.fareSelectPage = new fareSelect();
        world.fareSelectPage.assertFareSelect();
        world.fareSelectPage.selectFlight();
        world.fareSelectPage.selectFareOutgoing();
        world.fareSelectPage.selectReturnFlight();
        world.fareSelectPage.selectFareReturn();
        return world.fareSelectPage.continueButton();
    }

    function potentialTrip() {
        world.pt = new potential();
        world.pt.chooseSeats();
        return world.pt.checkOutButton();
    }

    function payment() {
        return browser.wait(EC.elementToBeClickable(locators.paymentPage().firstName), 6000);
    }

    return Promise.all([frontOfHouse(), flightSelection(), potentialTrip()]).then(function () {
        return payment();
    })

};

module.exports = potentialTripAndExtras;