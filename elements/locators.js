const locators = function () {

    this.homePage = function () {
        return {
            loginButton: $$('[id="myryanair-auth-login"]'),
            passwordField: $$('[ng-model="$ctrl.password"]'),
            email: $$('[ng-model="$ctrl.credentials.username"]'),
            city: element.all(by.model('$parent.value')),
            originCity: $$('[ng-model="$parent.value"]').get(0),
            destinationCity: $$('[ng-model="$parent.value"]').get(1),
            searchButton: element(by.css('[ng-click="searchFlights()"]'))
        };
    };

    this.fareSelectPage = function () {
        return {
            flightHeader: $$('[class="flight-header"]'),


            outgoingFlight: element.all(by.className('ranimate-flights-table flights-table__flight first')).first().element(by.className('flight-header__min-price hide-mobile')),

            //element.all(by.className('.ranimate-flights-table.flights-table__flight:not(.disabled)')).first().element(by.className('flight-header__min-price hide-mobile')),
            fareTypeOutgoing: element(by.className('flights-table-fares__fare fare-select standard')),
            returnFlight: $$('[class*="ranimate-flights-table"] [class="core-btn-primary"]').get(1),
            fareSelectedIcon: $$('[id="glyphs.circle-tick"]'),
            fareReturn: element(by.className('flights-table-fares__fare fare-select standard')),
            firstRow: $$('[class*="ranimate-flights-table"]').get(0),
            buttonContinue: element(by.className('core-btn-primary core-btn-block core-btn-medium')),
        };
    };

    this.potentialTrip = function () {
        return {
            nudgeBody: $$('.nudge-msg__body'),
            nudgeClose: $$('.nudge-msg__close'),
            seatSelected: $$('[class="seat-row-seat standard selected"]').get(0),
            extrasSection: $$('[class="extras-section extras-section--RECOMMENDED extras-section--RECOMMENDED-pt"]'),
            seatMapRows: $$('.seat-map-rows').get(0),
            seatsExtras: $$('[translate="trips.potential.card.SEATS.title"]'),
            seats1: $$('.seat-row-seat.standard:not(.reserved)'),
            nextButton: element(by.css('[class="core-btn-primary dialog-overlay-footer__ok-button"]')),
            sameSeats: $$('.same-seats').get(0),
            notSameSeats: $$('.core-btn-ghost').get(0),
            cancelButton: $$('[data-ref="dialog-overlay-footer-cancel-btn"]'),
            seats2: $$('.seat-row-seat.standard:not(.reserved)').get(0),
            confirmSeatsTitle: $$('.confirm-seats-title').get(0),
            confirmButton: element(by.css('[class="core-btn-primary dialog-overlay-footer__ok-button"]')),
            declinePB: $$('.priority-boarding-with-bags-popup__close').get(0),
            reservedSeatIcon: $$('.sm-legend__seat sm-legend__seat--reserved'),
            checkOut: $$('[data-ref="header-checkout-btn"]'),
            popUp: $$('.popup-msg__button').get(0),  //$$('.popup-msg'),
            closePopUp: $$('.popup-msg__close').get(0),
        }
    };

    this.paymentPage = function () {
        return {
            priceBreakdown: $$('.basket-sidebar'),
            coreCard: $$('.core-card').get(0),
            firstName: $$('[name*="firstName"]').get(0),
            firstField: element(by.css('[id*="firstName"]')),
            firstDropdown: $$('.companion-dropdown:not(.ng-hide1) .selected-passenger'),
            cardNum: $$('[id*="cardNumber"]'),
            cardType: $$('[id*="cardType"] [label="MasterCard"]'),
            expMonth: $$('[id*="expiryMonth"] [label="9"]'),
            expYear: $$('[name="expiryYear"] [label="2022"]'),
            cvvCode: $$('[name="securityCode"]'),
            cardName: $$('[name="cardHolderName"]'),
            address1: $$('[id="billingAddressAddressLine1"]'),
            address2: $$('[id="billingAddressAddressLine2"]'),
            city: $$('[id="billingAddressCity"]'),
            postalCode: $$('[id="billingAddressPostcode"]'),
            agreeButton: $$('[id*="acceptTerms"]'),
            countryDropdown: $$('[id="billingAddressCountry"] [label="Poland"]'),
            payNowButton: $$('[class*="core-btn-primary core-btn-medium"]'),
            errorMessage: $$('[class*="error prompt-text"]'),
            errorMessage2: $$('[translate="common.components.failed-payment.title"]').get(0),
            errorCard: $$('[translate="common.components.payment_forms.error_card_number_required"]'),

        }
    }

};

module.exports = locators;

